#nginx-fpm-phalcon
#created by mumei_himazin

FROM ubuntu:14.04.1


RUN apt-get update && apt-get -y upgrade

#install nginx
RUN apt-get -y install nginx
RUN echo 'daemon off;' >> /etc/nginx/nginx.conf
RUN mkdir /var/www && mkdir /var/www/public
RUN echo "<?php phpinfo(); ?>" > /var/www/public/index.php

#install php and php-fpm
RUN apt-get -y install php5 php5-dev php5-fpm
ADD fastcgi_params /etc/nginx/fastcgi_params
RUN rm /etc/nginx/sites-enabled/default
RUN mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default.backup
ADD default /etc/nginx/sites-available/default
RUN ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default


#install php-extentions
RUN apt-get -y install php5-cli php-pear php5-mysql php5-mongo php5-mcrypt
RUN ln -s /etc/php5/mods-available/mcrypt.ini /etc/php5/fpm/conf.d/20-mcrypt.ini
RUN ln -s /etc/php5/mods-available/mcrypt.ini /etc/php5/cli/conf.d/20-mcrypt.ini



#build and install phalcon
RUN apt-get -y install git gcc libpcre3-dev
RUN git clone --depth=1 git://github.com/phalcon/cphalcon.git
RUN cd cphalcon/build && ./install;
ADD phalcon.ini /etc/php5/mods-available/phalcon.ini
RUN ln -s /etc/php5/mods-available/phalcon.ini /etc/php5/fpm/conf.d/30-phalcon.ini
RUN ln -s /etc/php5/mods-available/phalcon.ini /etc/php5/cli/conf.d/30-phalcon.ini

#start shell script
ADD start.sh /start.sh
RUN chmod 744 /start.sh

EXPOSE 80

CMD ./start.sh
