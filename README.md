#docker-nginx-fpm-phalcon

## installed

- ubuntu 14.04.1
- nginx 
- php5-fpm
- phalcon extension
- php5-cli
- php5-mysql
- php5-mongo
- php5-mcrypt

## Build
	docker build --rm -t <image_name> docker-nginx-fpm-phalcon

## Usage:
	docker run -d -p <port>:80 --name <name> <image_name>
